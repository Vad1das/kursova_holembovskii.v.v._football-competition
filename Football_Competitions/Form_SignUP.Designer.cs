﻿namespace Football_Competitions
{
    partial class Form_SignUP
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_SignUP));
            this.label_SignUP = new System.Windows.Forms.Label();
            this.textBox_SignUP_Name = new System.Windows.Forms.TextBox();
            this.textBox_SignUP_Surname = new System.Windows.Forms.TextBox();
            this.textBox_SignUP_Login = new System.Windows.Forms.TextBox();
            this.textBox_SignUP_Password = new System.Windows.Forms.TextBox();
            this.label_SignUP_Name = new System.Windows.Forms.Label();
            this.label_SignUP_Surname = new System.Windows.Forms.Label();
            this.label_SignUP_Login = new System.Windows.Forms.Label();
            this.label_SignUP_Password = new System.Windows.Forms.Label();
            this.button_SignUP_SignUP = new System.Windows.Forms.Button();
            this.linkLabel_SignUP_SignIN = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label_SignUP
            // 
            this.label_SignUP.AutoSize = true;
            this.label_SignUP.BackColor = System.Drawing.Color.Transparent;
            this.label_SignUP.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_SignUP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_SignUP.Location = new System.Drawing.Point(200, 30);
            this.label_SignUP.Name = "label_SignUP";
            this.label_SignUP.Size = new System.Drawing.Size(395, 55);
            this.label_SignUP.TabIndex = 0;
            this.label_SignUP.Text = "REGISTRATION";
            // 
            // textBox_SignUP_Name
            // 
            this.textBox_SignUP_Name.BackColor = System.Drawing.Color.White;
            this.textBox_SignUP_Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_SignUP_Name.ForeColor = System.Drawing.Color.Black;
            this.textBox_SignUP_Name.Location = new System.Drawing.Point(369, 139);
            this.textBox_SignUP_Name.MaxLength = 20;
            this.textBox_SignUP_Name.Multiline = true;
            this.textBox_SignUP_Name.Name = "textBox_SignUP_Name";
            this.textBox_SignUP_Name.Size = new System.Drawing.Size(210, 30);
            this.textBox_SignUP_Name.TabIndex = 1;
            this.textBox_SignUP_Name.Enter += new System.EventHandler(this.textBox_SignUP_Name_Enter);
            this.textBox_SignUP_Name.Leave += new System.EventHandler(this.textBox_SignUP_Name_Leave);
            // 
            // textBox_SignUP_Surname
            // 
            this.textBox_SignUP_Surname.BackColor = System.Drawing.Color.White;
            this.textBox_SignUP_Surname.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_SignUP_Surname.ForeColor = System.Drawing.Color.Black;
            this.textBox_SignUP_Surname.Location = new System.Drawing.Point(369, 185);
            this.textBox_SignUP_Surname.MaxLength = 20;
            this.textBox_SignUP_Surname.Multiline = true;
            this.textBox_SignUP_Surname.Name = "textBox_SignUP_Surname";
            this.textBox_SignUP_Surname.Size = new System.Drawing.Size(210, 30);
            this.textBox_SignUP_Surname.TabIndex = 2;
            this.textBox_SignUP_Surname.Enter += new System.EventHandler(this.textBox_SignUP_Surname_Enter);
            this.textBox_SignUP_Surname.Leave += new System.EventHandler(this.textBox_SignUP_Surname_Leave);
            // 
            // textBox_SignUP_Login
            // 
            this.textBox_SignUP_Login.BackColor = System.Drawing.Color.White;
            this.textBox_SignUP_Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_SignUP_Login.ForeColor = System.Drawing.Color.Black;
            this.textBox_SignUP_Login.Location = new System.Drawing.Point(369, 232);
            this.textBox_SignUP_Login.MaxLength = 15;
            this.textBox_SignUP_Login.Multiline = true;
            this.textBox_SignUP_Login.Name = "textBox_SignUP_Login";
            this.textBox_SignUP_Login.Size = new System.Drawing.Size(210, 30);
            this.textBox_SignUP_Login.TabIndex = 3;
            this.textBox_SignUP_Login.Enter += new System.EventHandler(this.textBox_SignUP_Login_Enter);
            this.textBox_SignUP_Login.Leave += new System.EventHandler(this.textBox_SignUP_Login_Leave);
            // 
            // textBox_SignUP_Password
            // 
            this.textBox_SignUP_Password.BackColor = System.Drawing.Color.White;
            this.textBox_SignUP_Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_SignUP_Password.ForeColor = System.Drawing.Color.Black;
            this.textBox_SignUP_Password.Location = new System.Drawing.Point(369, 280);
            this.textBox_SignUP_Password.MaxLength = 16;
            this.textBox_SignUP_Password.Multiline = true;
            this.textBox_SignUP_Password.Name = "textBox_SignUP_Password";
            this.textBox_SignUP_Password.Size = new System.Drawing.Size(210, 30);
            this.textBox_SignUP_Password.TabIndex = 4;
            this.textBox_SignUP_Password.Enter += new System.EventHandler(this.textBox_SignUP_Password_Enter);
            this.textBox_SignUP_Password.Leave += new System.EventHandler(this.textBox_SignUP_Password_Leave);
            // 
            // label_SignUP_Name
            // 
            this.label_SignUP_Name.AutoSize = true;
            this.label_SignUP_Name.BackColor = System.Drawing.Color.Transparent;
            this.label_SignUP_Name.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_SignUP_Name.ForeColor = System.Drawing.Color.LemonChiffon;
            this.label_SignUP_Name.Location = new System.Drawing.Point(250, 146);
            this.label_SignUP_Name.Name = "label_SignUP_Name";
            this.label_SignUP_Name.Size = new System.Drawing.Size(113, 23);
            this.label_SignUP_Name.TabIndex = 5;
            this.label_SignUP_Name.Text = "Ваше ім\'я:";
            // 
            // label_SignUP_Surname
            // 
            this.label_SignUP_Surname.AutoSize = true;
            this.label_SignUP_Surname.BackColor = System.Drawing.Color.Transparent;
            this.label_SignUP_Surname.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_SignUP_Surname.ForeColor = System.Drawing.Color.LemonChiffon;
            this.label_SignUP_Surname.Location = new System.Drawing.Point(194, 192);
            this.label_SignUP_Surname.Name = "label_SignUP_Surname";
            this.label_SignUP_Surname.Size = new System.Drawing.Size(169, 23);
            this.label_SignUP_Surname.TabIndex = 6;
            this.label_SignUP_Surname.Text = "Ваше прізвище:";
            // 
            // label_SignUP_Login
            // 
            this.label_SignUP_Login.AutoSize = true;
            this.label_SignUP_Login.BackColor = System.Drawing.Color.Transparent;
            this.label_SignUP_Login.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_SignUP_Login.ForeColor = System.Drawing.Color.LemonChiffon;
            this.label_SignUP_Login.Location = new System.Drawing.Point(213, 239);
            this.label_SignUP_Login.Name = "label_SignUP_Login";
            this.label_SignUP_Login.Size = new System.Drawing.Size(150, 23);
            this.label_SignUP_Login.TabIndex = 7;
            this.label_SignUP_Login.Text = "Введіть логін:";
            // 
            // label_SignUP_Password
            // 
            this.label_SignUP_Password.AutoSize = true;
            this.label_SignUP_Password.BackColor = System.Drawing.Color.Transparent;
            this.label_SignUP_Password.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_SignUP_Password.ForeColor = System.Drawing.Color.LemonChiffon;
            this.label_SignUP_Password.Location = new System.Drawing.Point(194, 287);
            this.label_SignUP_Password.Name = "label_SignUP_Password";
            this.label_SignUP_Password.Size = new System.Drawing.Size(169, 23);
            this.label_SignUP_Password.TabIndex = 8;
            this.label_SignUP_Password.Text = "Введіть пароль:";
            // 
            // button_SignUP_SignUP
            // 
            this.button_SignUP_SignUP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button_SignUP_SignUP.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_SignUP_SignUP.Location = new System.Drawing.Point(369, 338);
            this.button_SignUP_SignUP.Name = "button_SignUP_SignUP";
            this.button_SignUP_SignUP.Size = new System.Drawing.Size(140, 61);
            this.button_SignUP_SignUP.TabIndex = 9;
            this.button_SignUP_SignUP.Text = "Sign Up";
            this.button_SignUP_SignUP.UseVisualStyleBackColor = false;
            this.button_SignUP_SignUP.Click += new System.EventHandler(this.button_SignUP_SignUP_Click);
            // 
            // linkLabel_SignUP_SignIN
            // 
            this.linkLabel_SignUP_SignIN.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.linkLabel_SignUP_SignIN.AutoSize = true;
            this.linkLabel_SignUP_SignIN.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel_SignUP_SignIN.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel_SignUP_SignIN.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.linkLabel_SignUP_SignIN.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.linkLabel_SignUP_SignIN.Location = new System.Drawing.Point(250, 422);
            this.linkLabel_SignUP_SignIN.Name = "linkLabel_SignUP_SignIN";
            this.linkLabel_SignUP_SignIN.Size = new System.Drawing.Size(295, 18);
            this.linkLabel_SignUP_SignIN.TabIndex = 10;
            this.linkLabel_SignUP_SignIN.TabStop = true;
            this.linkLabel_SignUP_SignIN.Text = "I already have an account. Sign in!";
            this.linkLabel_SignUP_SignIN.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.linkLabel_SignUP_SignIN.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_SignUP_SignIN_LinkClicked);
            // 
            // Form_SignUP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackgroundImage = global::Football_Competitions.Properties.Resources.Background_Grass;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 661);
            this.Controls.Add(this.linkLabel_SignUP_SignIN);
            this.Controls.Add(this.button_SignUP_SignUP);
            this.Controls.Add(this.label_SignUP_Password);
            this.Controls.Add(this.label_SignUP_Login);
            this.Controls.Add(this.label_SignUP_Surname);
            this.Controls.Add(this.label_SignUP_Name);
            this.Controls.Add(this.textBox_SignUP_Password);
            this.Controls.Add(this.textBox_SignUP_Login);
            this.Controls.Add(this.textBox_SignUP_Surname);
            this.Controls.Add(this.textBox_SignUP_Name);
            this.Controls.Add(this.label_SignUP);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(800, 700);
            this.MinimumSize = new System.Drawing.Size(800, 700);
            this.Name = "Form_SignUP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Football — Реєстраціїя";
            this.Load += new System.EventHandler(this.Form_SignUP_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_SignUP;
        private System.Windows.Forms.TextBox textBox_SignUP_Name;
        private System.Windows.Forms.TextBox textBox_SignUP_Surname;
        private System.Windows.Forms.TextBox textBox_SignUP_Login;
        private System.Windows.Forms.TextBox textBox_SignUP_Password;
        private System.Windows.Forms.Label label_SignUP_Name;
        private System.Windows.Forms.Label label_SignUP_Surname;
        private System.Windows.Forms.Label label_SignUP_Login;
        private System.Windows.Forms.Label label_SignUP_Password;
        private System.Windows.Forms.Button button_SignUP_SignUP;
        private System.Windows.Forms.LinkLabel linkLabel_SignUP_SignIN;
    }
}

