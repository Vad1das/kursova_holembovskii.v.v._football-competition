﻿SET IDENTITY_INSERT [dbo].[Users] ON
INSERT INTO [dbo].[Users] ([Id], [Login], [Password], [Name], [Surname], [Admin]) VALUES
(2, N'NatusVincere2', N'DontLoseMid', N'Данило', N'Ішутін', 0),
(3, N'NatusVincere3', N'FamilyInNavi', N'Олександр', N'Костилев', 0),
(4, N'VirtusPro1', N'HeadNShoulders', N'Роман', N'Кушнарев', 0),
(5, N'VirtusPro2', N'151617181920', N'Володимир', N'Миненко', 0),
(6, N'VirtusPro3', N'DiamondEye1920', N'Сергій', N'Гламазда', 0),
(7, N'admin1', N'20022002', N'Вадим', N'Голембовський', 1),
(8, N'admin2', N'AdminIsHere', N'Владислав', N'Голембовський', 1),
(9, N'user1', N'Anonimchik', N'Богдан', N'Василенко', 0),
(10, N'user2', N'Incognito666', N'Гоша', N'Дударь', 0)
SET IDENTITY_INSERT [dbo].[Users] OFF
