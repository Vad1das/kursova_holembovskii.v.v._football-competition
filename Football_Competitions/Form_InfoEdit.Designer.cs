﻿namespace Football_Competitions
{
    partial class Form_InfoEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_InfoEdit));
            this.menuStrip_InfoEdit = new System.Windows.Forms.MenuStrip();
            this.Exit2_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit2_1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit2_2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit2_3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit2_4ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit2_5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Info2_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Refresh2_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Refresh2_1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Refresh2_2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Edit2_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Delete2_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Delete2_1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Delete2_2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Help2_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label_InfoEdit_Information = new System.Windows.Forms.Label();
            this.numericUpDown_InfoEdit_Add = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_InfoEdit_Select = new System.Windows.Forms.NumericUpDown();
            this.button_InfoEdit_Add = new System.Windows.Forms.Button();
            this.button_InfoEdit_Find = new System.Windows.Forms.Button();
            this.label_InfoEdit_Add = new System.Windows.Forms.Label();
            this.label_InfoEdit_Select = new System.Windows.Forms.Label();
            this.dataGridView_InfoEdit = new System.Windows.Forms.DataGridView();
            this.menuStrip_InfoEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_InfoEdit_Add)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_InfoEdit_Select)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_InfoEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip_InfoEdit
            // 
            this.menuStrip_InfoEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.menuStrip_InfoEdit.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Exit2_ToolStripMenuItem,
            this.Info2_ToolStripMenuItem,
            this.Refresh2_ToolStripMenuItem,
            this.Edit2_ToolStripMenuItem,
            this.Delete2_ToolStripMenuItem,
            this.Help2_ToolStripMenuItem});
            this.menuStrip_InfoEdit.Location = new System.Drawing.Point(0, 0);
            this.menuStrip_InfoEdit.Name = "menuStrip_InfoEdit";
            this.menuStrip_InfoEdit.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip_InfoEdit.Size = new System.Drawing.Size(984, 24);
            this.menuStrip_InfoEdit.TabIndex = 1;
            this.menuStrip_InfoEdit.Text = "menuStrip_InfoEdit";
            // 
            // Exit2_ToolStripMenuItem
            // 
            this.Exit2_ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Exit2_ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Exit2_1ToolStripMenuItem,
            this.Exit2_2ToolStripMenuItem,
            this.Exit2_3ToolStripMenuItem,
            this.Exit2_4ToolStripMenuItem,
            this.Exit2_5ToolStripMenuItem});
            this.Exit2_ToolStripMenuItem.Name = "Exit2_ToolStripMenuItem";
            this.Exit2_ToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.Exit2_ToolStripMenuItem.Text = "Exit";
            // 
            // Exit2_1ToolStripMenuItem
            // 
            this.Exit2_1ToolStripMenuItem.Name = "Exit2_1ToolStripMenuItem";
            this.Exit2_1ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.Exit2_1ToolStripMenuItem.Text = "Вийти з програми";
            this.Exit2_1ToolStripMenuItem.Click += new System.EventHandler(this.Exit2_1ToolStripMenuItem_Click);
            // 
            // Exit2_2ToolStripMenuItem
            // 
            this.Exit2_2ToolStripMenuItem.Name = "Exit2_2ToolStripMenuItem";
            this.Exit2_2ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.Exit2_2ToolStripMenuItem.Text = "В звичайний режим";
            this.Exit2_2ToolStripMenuItem.Click += new System.EventHandler(this.Exit2_2ToolStripMenuItem_Click);
            // 
            // Exit2_3ToolStripMenuItem
            // 
            this.Exit2_3ToolStripMenuItem.Name = "Exit2_3ToolStripMenuItem";
            this.Exit2_3ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.Exit2_3ToolStripMenuItem.Text = "В меню входу";
            this.Exit2_3ToolStripMenuItem.Click += new System.EventHandler(this.Exit2_3ToolStripMenuItem_Click);
            // 
            // Exit2_4ToolStripMenuItem
            // 
            this.Exit2_4ToolStripMenuItem.Name = "Exit2_4ToolStripMenuItem";
            this.Exit2_4ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.Exit2_4ToolStripMenuItem.Text = "В меню реєстрації";
            this.Exit2_4ToolStripMenuItem.Click += new System.EventHandler(this.Exit2_4ToolStripMenuItem_Click);
            // 
            // Exit2_5ToolStripMenuItem
            // 
            this.Exit2_5ToolStripMenuItem.Name = "Exit2_5ToolStripMenuItem";
            this.Exit2_5ToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.Exit2_5ToolStripMenuItem.Text = "В форму пошуку";
            this.Exit2_5ToolStripMenuItem.Click += new System.EventHandler(this.Exit2_5ToolStripMenuItem_Click);
            // 
            // Info2_ToolStripMenuItem
            // 
            this.Info2_ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Info2_ToolStripMenuItem.Name = "Info2_ToolStripMenuItem";
            this.Info2_ToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Info2_ToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.Info2_ToolStripMenuItem.Text = "Info";
            this.Info2_ToolStripMenuItem.Click += new System.EventHandler(this.Info2_ToolStripMenuItem_Click);
            // 
            // Refresh2_ToolStripMenuItem
            // 
            this.Refresh2_ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Refresh2_1ToolStripMenuItem,
            this.Refresh2_2ToolStripMenuItem});
            this.Refresh2_ToolStripMenuItem.Name = "Refresh2_ToolStripMenuItem";
            this.Refresh2_ToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.Refresh2_ToolStripMenuItem.Text = "Refresh";
            // 
            // Refresh2_1ToolStripMenuItem
            // 
            this.Refresh2_1ToolStripMenuItem.Name = "Refresh2_1ToolStripMenuItem";
            this.Refresh2_1ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.Refresh2_1ToolStripMenuItem.Text = "FROM server";
            this.Refresh2_1ToolStripMenuItem.Click += new System.EventHandler(this.Refresh2_1ToolStripMenuItem_Click);
            // 
            // Refresh2_2ToolStripMenuItem
            // 
            this.Refresh2_2ToolStripMenuItem.Name = "Refresh2_2ToolStripMenuItem";
            this.Refresh2_2ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.Refresh2_2ToolStripMenuItem.Text = "ON server";
            this.Refresh2_2ToolStripMenuItem.Click += new System.EventHandler(this.Refresh2_2ToolStripMenuItem_Click);
            // 
            // Edit2_ToolStripMenuItem
            // 
            this.Edit2_ToolStripMenuItem.Name = "Edit2_ToolStripMenuItem";
            this.Edit2_ToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.Edit2_ToolStripMenuItem.Text = "Edit";
            this.Edit2_ToolStripMenuItem.Click += new System.EventHandler(this.Edit2_ToolStripMenuItem_Click);
            // 
            // Delete2_ToolStripMenuItem
            // 
            this.Delete2_ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Delete2_1ToolStripMenuItem,
            this.Delete2_2ToolStripMenuItem});
            this.Delete2_ToolStripMenuItem.Name = "Delete2_ToolStripMenuItem";
            this.Delete2_ToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.Delete2_ToolStripMenuItem.Text = "Delete";
            // 
            // Delete2_1ToolStripMenuItem
            // 
            this.Delete2_1ToolStripMenuItem.Name = "Delete2_1ToolStripMenuItem";
            this.Delete2_1ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.Delete2_1ToolStripMenuItem.Text = "All";
            this.Delete2_1ToolStripMenuItem.Click += new System.EventHandler(this.Delete2_1ToolStripMenuItem_Click);
            // 
            // Delete2_2ToolStripMenuItem
            // 
            this.Delete2_2ToolStripMenuItem.Name = "Delete2_2ToolStripMenuItem";
            this.Delete2_2ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.Delete2_2ToolStripMenuItem.Text = "Choosen ";
            this.Delete2_2ToolStripMenuItem.Click += new System.EventHandler(this.Delete2_2ToolStripMenuItem_Click);
            // 
            // Help2_ToolStripMenuItem
            // 
            this.Help2_ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Help2_ToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Help2_ToolStripMenuItem.Name = "Help2_ToolStripMenuItem";
            this.Help2_ToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.Help2_ToolStripMenuItem.Text = "Help";
            this.Help2_ToolStripMenuItem.Click += new System.EventHandler(this.Help2_ToolStripMenuItem_Click);
            // 
            // label_InfoEdit_Information
            // 
            this.label_InfoEdit_Information.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label_InfoEdit_Information.AutoSize = true;
            this.label_InfoEdit_Information.BackColor = System.Drawing.Color.Transparent;
            this.label_InfoEdit_Information.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_InfoEdit_Information.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_InfoEdit_Information.Location = new System.Drawing.Point(305, 45);
            this.label_InfoEdit_Information.Name = "label_InfoEdit_Information";
            this.label_InfoEdit_Information.Size = new System.Drawing.Size(370, 55);
            this.label_InfoEdit_Information.TabIndex = 2;
            this.label_InfoEdit_Information.Text = "INFORMATION";
            // 
            // numericUpDown_InfoEdit_Add
            // 
            this.numericUpDown_InfoEdit_Add.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numericUpDown_InfoEdit_Add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.numericUpDown_InfoEdit_Add.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericUpDown_InfoEdit_Add.Location = new System.Drawing.Point(481, 114);
            this.numericUpDown_InfoEdit_Add.Name = "numericUpDown_InfoEdit_Add";
            this.numericUpDown_InfoEdit_Add.Size = new System.Drawing.Size(120, 22);
            this.numericUpDown_InfoEdit_Add.TabIndex = 4;
            // 
            // numericUpDown_InfoEdit_Select
            // 
            this.numericUpDown_InfoEdit_Select.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numericUpDown_InfoEdit_Select.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.numericUpDown_InfoEdit_Select.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericUpDown_InfoEdit_Select.Location = new System.Drawing.Point(481, 151);
            this.numericUpDown_InfoEdit_Select.Name = "numericUpDown_InfoEdit_Select";
            this.numericUpDown_InfoEdit_Select.Size = new System.Drawing.Size(120, 22);
            this.numericUpDown_InfoEdit_Select.TabIndex = 5;
            // 
            // button_InfoEdit_Add
            // 
            this.button_InfoEdit_Add.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button_InfoEdit_Add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button_InfoEdit_Add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_InfoEdit_Add.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_InfoEdit_Add.Location = new System.Drawing.Point(618, 113);
            this.button_InfoEdit_Add.Name = "button_InfoEdit_Add";
            this.button_InfoEdit_Add.Size = new System.Drawing.Size(96, 22);
            this.button_InfoEdit_Add.TabIndex = 6;
            this.button_InfoEdit_Add.Text = "Add";
            this.button_InfoEdit_Add.UseVisualStyleBackColor = false;
            this.button_InfoEdit_Add.Click += new System.EventHandler(this.button_InfoEdit_Add_Click);
            // 
            // button_InfoEdit_Find
            // 
            this.button_InfoEdit_Find.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button_InfoEdit_Find.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button_InfoEdit_Find.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_InfoEdit_Find.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_InfoEdit_Find.Location = new System.Drawing.Point(618, 151);
            this.button_InfoEdit_Find.Name = "button_InfoEdit_Find";
            this.button_InfoEdit_Find.Size = new System.Drawing.Size(96, 22);
            this.button_InfoEdit_Find.TabIndex = 7;
            this.button_InfoEdit_Find.Text = "Find";
            this.button_InfoEdit_Find.UseVisualStyleBackColor = false;
            this.button_InfoEdit_Find.Click += new System.EventHandler(this.button_InfoEdit_Find_Click);
            // 
            // label_InfoEdit_Add
            // 
            this.label_InfoEdit_Add.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label_InfoEdit_Add.AutoSize = true;
            this.label_InfoEdit_Add.BackColor = System.Drawing.Color.Transparent;
            this.label_InfoEdit_Add.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_InfoEdit_Add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_InfoEdit_Add.Location = new System.Drawing.Point(222, 116);
            this.label_InfoEdit_Add.Name = "label_InfoEdit_Add";
            this.label_InfoEdit_Add.Size = new System.Drawing.Size(253, 20);
            this.label_InfoEdit_Add.TabIndex = 8;
            this.label_InfoEdit_Add.Text = "Додати таку к-ть записів:";
            // 
            // label_InfoEdit_Select
            // 
            this.label_InfoEdit_Select.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label_InfoEdit_Select.AutoSize = true;
            this.label_InfoEdit_Select.BackColor = System.Drawing.Color.Transparent;
            this.label_InfoEdit_Select.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_InfoEdit_Select.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_InfoEdit_Select.Location = new System.Drawing.Point(222, 151);
            this.label_InfoEdit_Select.Name = "label_InfoEdit_Select";
            this.label_InfoEdit_Select.Size = new System.Drawing.Size(115, 20);
            this.label_InfoEdit_Select.TabIndex = 9;
            this.label_InfoEdit_Select.Text = "Введіть ID:";
            // 
            // dataGridView_InfoEdit
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView_InfoEdit.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_InfoEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_InfoEdit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_InfoEdit.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_InfoEdit.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_InfoEdit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_InfoEdit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_InfoEdit.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_InfoEdit.Location = new System.Drawing.Point(12, 191);
            this.dataGridView_InfoEdit.Name = "dataGridView_InfoEdit";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_InfoEdit.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView_InfoEdit.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView_InfoEdit.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dataGridView_InfoEdit.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dataGridView_InfoEdit.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dataGridView_InfoEdit.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dataGridView_InfoEdit.Size = new System.Drawing.Size(960, 558);
            this.dataGridView_InfoEdit.TabIndex = 14;
            // 
            // Form_InfoEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Football_Competitions.Properties.Resources.Background_Grass;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(984, 761);
            this.Controls.Add(this.dataGridView_InfoEdit);
            this.Controls.Add(this.label_InfoEdit_Select);
            this.Controls.Add(this.label_InfoEdit_Add);
            this.Controls.Add(this.button_InfoEdit_Find);
            this.Controls.Add(this.button_InfoEdit_Add);
            this.Controls.Add(this.numericUpDown_InfoEdit_Select);
            this.Controls.Add(this.numericUpDown_InfoEdit_Add);
            this.Controls.Add(this.label_InfoEdit_Information);
            this.Controls.Add(this.menuStrip_InfoEdit);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 700);
            this.Name = "Form_InfoEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Football — Редагування";
            this.Shown += new System.EventHandler(this.Form_InfoEdit_Shown);
            this.menuStrip_InfoEdit.ResumeLayout(false);
            this.menuStrip_InfoEdit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_InfoEdit_Add)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_InfoEdit_Select)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_InfoEdit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip_InfoEdit;
        private System.Windows.Forms.ToolStripMenuItem Exit2_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit2_1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit2_3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit2_4ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Info2_ToolStripMenuItem;
        private System.Windows.Forms.Label label_InfoEdit_Information;
        private System.Windows.Forms.NumericUpDown numericUpDown_InfoEdit_Add;
        private System.Windows.Forms.NumericUpDown numericUpDown_InfoEdit_Select;
        private System.Windows.Forms.Button button_InfoEdit_Add;
        private System.Windows.Forms.Button button_InfoEdit_Find;
        private System.Windows.Forms.Label label_InfoEdit_Add;
        private System.Windows.Forms.Label label_InfoEdit_Select;
        private System.Windows.Forms.ToolStripMenuItem Exit2_2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Refresh2_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Refresh2_1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Refresh2_2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Edit2_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Delete2_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Delete2_1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Delete2_2ToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridView_InfoEdit;
        private System.Windows.Forms.ToolStripMenuItem Help2_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit2_5ToolStripMenuItem;
    }
}