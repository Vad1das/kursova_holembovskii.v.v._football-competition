﻿namespace Football_Competitions
{
    partial class Form_SignIN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_SignIN));
            this.label_SignIN = new System.Windows.Forms.Label();
            this.textBox_SignIN_Login = new System.Windows.Forms.TextBox();
            this.textBox_SignIN_Password = new System.Windows.Forms.TextBox();
            this.label_SignIN_Login = new System.Windows.Forms.Label();
            this.label_SignIN_Password = new System.Windows.Forms.Label();
            this.button_SignIN_SignIN = new System.Windows.Forms.Button();
            this.linkLabel_SignIN_SignUP = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label_SignIN
            // 
            this.label_SignIN.AutoSize = true;
            this.label_SignIN.BackColor = System.Drawing.Color.Transparent;
            this.label_SignIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_SignIN.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_SignIN.Location = new System.Drawing.Point(260, 30);
            this.label_SignIN.Name = "label_SignIN";
            this.label_SignIN.Size = new System.Drawing.Size(209, 55);
            this.label_SignIN.TabIndex = 1;
            this.label_SignIN.Text = "SIGN IN";
            // 
            // textBox_SignIN_Login
            // 
            this.textBox_SignIN_Login.BackColor = System.Drawing.Color.White;
            this.textBox_SignIN_Login.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_SignIN_Login.ForeColor = System.Drawing.Color.Black;
            this.textBox_SignIN_Login.Location = new System.Drawing.Point(400, 145);
            this.textBox_SignIN_Login.MaxLength = 15;
            this.textBox_SignIN_Login.Multiline = true;
            this.textBox_SignIN_Login.Name = "textBox_SignIN_Login";
            this.textBox_SignIN_Login.Size = new System.Drawing.Size(210, 30);
            this.textBox_SignIN_Login.TabIndex = 2;
            this.textBox_SignIN_Login.Enter += new System.EventHandler(this.textBox_SignIN_Login_Enter);
            this.textBox_SignIN_Login.Leave += new System.EventHandler(this.textBox_SignIN_Login_Leave);
            // 
            // textBox_SignIN_Password
            // 
            this.textBox_SignIN_Password.BackColor = System.Drawing.Color.White;
            this.textBox_SignIN_Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_SignIN_Password.ForeColor = System.Drawing.Color.Black;
            this.textBox_SignIN_Password.Location = new System.Drawing.Point(400, 195);
            this.textBox_SignIN_Password.MaxLength = 16;
            this.textBox_SignIN_Password.Multiline = true;
            this.textBox_SignIN_Password.Name = "textBox_SignIN_Password";
            this.textBox_SignIN_Password.Size = new System.Drawing.Size(210, 30);
            this.textBox_SignIN_Password.TabIndex = 3;
            this.textBox_SignIN_Password.Enter += new System.EventHandler(this.textBox_SignIN_Password_Enter);
            this.textBox_SignIN_Password.Leave += new System.EventHandler(this.textBox_SignIN_Passeord_Leave);
            // 
            // label_SignIN_Login
            // 
            this.label_SignIN_Login.AutoSize = true;
            this.label_SignIN_Login.BackColor = System.Drawing.Color.Transparent;
            this.label_SignIN_Login.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_SignIN_Login.ForeColor = System.Drawing.Color.LemonChiffon;
            this.label_SignIN_Login.Location = new System.Drawing.Point(150, 150);
            this.label_SignIN_Login.Name = "label_SignIN_Login";
            this.label_SignIN_Login.Size = new System.Drawing.Size(150, 23);
            this.label_SignIN_Login.TabIndex = 8;
            this.label_SignIN_Login.Text = "Введіть логін:";
            // 
            // label_SignIN_Password
            // 
            this.label_SignIN_Password.AutoSize = true;
            this.label_SignIN_Password.BackColor = System.Drawing.Color.Transparent;
            this.label_SignIN_Password.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_SignIN_Password.ForeColor = System.Drawing.Color.LemonChiffon;
            this.label_SignIN_Password.Location = new System.Drawing.Point(150, 200);
            this.label_SignIN_Password.Name = "label_SignIN_Password";
            this.label_SignIN_Password.Size = new System.Drawing.Size(169, 23);
            this.label_SignIN_Password.TabIndex = 9;
            this.label_SignIN_Password.Text = "Введіть пароль:";
            // 
            // button_SignIN_SignIN
            // 
            this.button_SignIN_SignIN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button_SignIN_SignIN.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_SignIN_SignIN.Location = new System.Drawing.Point(300, 300);
            this.button_SignIN_SignIN.Name = "button_SignIN_SignIN";
            this.button_SignIN_SignIN.Size = new System.Drawing.Size(140, 61);
            this.button_SignIN_SignIN.TabIndex = 10;
            this.button_SignIN_SignIN.Text = "Sign In";
            this.button_SignIN_SignIN.UseVisualStyleBackColor = false;
            this.button_SignIN_SignIN.Click += new System.EventHandler(this.button_SignIN_SignIN_Click);
            // 
            // linkLabel_SignIN_SignUP
            // 
            this.linkLabel_SignIN_SignUP.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.linkLabel_SignIN_SignUP.AutoSize = true;
            this.linkLabel_SignIN_SignUP.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel_SignIN_SignUP.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel_SignIN_SignUP.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.linkLabel_SignIN_SignUP.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.linkLabel_SignIN_SignUP.Location = new System.Drawing.Point(230, 400);
            this.linkLabel_SignIN_SignUP.Name = "linkLabel_SignIN_SignUP";
            this.linkLabel_SignIN_SignUP.Size = new System.Drawing.Size(281, 18);
            this.linkLabel_SignIN_SignUP.TabIndex = 11;
            this.linkLabel_SignIN_SignUP.TabStop = true;
            this.linkLabel_SignIN_SignUP.Text = "I don\'t have an account. Sign up!";
            this.linkLabel_SignIN_SignUP.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.linkLabel_SignIN_SignUP.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_SignIN_SignUP_LinkClicked);
            // 
            // Form_SignIN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Football_Competitions.Properties.Resources.Background_Grass;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 661);
            this.Controls.Add(this.linkLabel_SignIN_SignUP);
            this.Controls.Add(this.button_SignIN_SignIN);
            this.Controls.Add(this.label_SignIN_Password);
            this.Controls.Add(this.label_SignIN_Login);
            this.Controls.Add(this.textBox_SignIN_Password);
            this.Controls.Add(this.textBox_SignIN_Login);
            this.Controls.Add(this.label_SignIN);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(800, 700);
            this.MinimumSize = new System.Drawing.Size(800, 700);
            this.Name = "Form_SignIN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Football — Вхід";
            this.Load += new System.EventHandler(this.Form_SignIN_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_SignIN;
        private System.Windows.Forms.TextBox textBox_SignIN_Login;
        private System.Windows.Forms.TextBox textBox_SignIN_Password;
        private System.Windows.Forms.Label label_SignIN_Login;
        private System.Windows.Forms.Label label_SignIN_Password;
        private System.Windows.Forms.Button button_SignIN_SignIN;
        private System.Windows.Forms.LinkLabel linkLabel_SignIN_SignUP;
    }
}