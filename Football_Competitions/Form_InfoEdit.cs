﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Football_Competitions
{
    public partial class Form_InfoEdit : Form
    {
        private List<Fields> _data = new List<Fields>();
        private User user;

        public Form_InfoEdit()
        {
            InitializeComponent();
        }

        //ToolStrip - Info, Help and Different exits
        private void Info2_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string infostring = "Інформаційно-пошукова система: футбольні змагання.\n" + "Звичайний користувач може переглядати, шукати інформацію. Адмін може її ще змінювати, видаляти.\n"
                + "Автор: Голембовський Вадим КІ-19-1[1].\n";
            MessageBox.Show(infostring, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void Help2_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string helpstring = "Це форма редагування даних, доступ до якої є тільки в адміністраторів.\n" + "Невеличкі підказки.\n"
                + "1.) Для редагування інформації, необхідно виділити запис(и) цілком. Потім вже натиснути на кнопку Edit\n"
                + "2.) Для додання інформації, виберіть к-ть рядків, що буде додана. З'явиться обрана к-ть порожніх рядків.\n"
                + "Впишіть у них дані (в ID дані вписувати не потрібно, навіть якщо вам це вдасться :)). Потім вже натиснути на кнопки: Refresh -> ON server\n"
                + "виділивши весь рядок(рядки). Для перегляду всієї інформації натисніть Refresh -> FROM server\n"
                + "3.) Для пошуку запису за ID виберіть чи впишіть значення та натисність на кнопку Select.\n"
                + "Тоді база даних видасть вам тільки запис з цим ID.\n" + "Для того, щоб всі дані знову відображалися натисніть на Refresh -> FROM server.";
            MessageBox.Show(helpstring, "Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void Exit2_1ToolStripMenuItem_Click(object sender, EventArgs e) { Application.Exit(); }
        private void Exit2_2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вийти з адмін режиму?", "Увага!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Form_InfoShow form = new Form_InfoShow();
                this.Hide(); form.Show();
            }
        }
        private void Exit2_3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_SignIN form = new Form_SignIN();
            this.Hide(); form.Show();
        }
        private void Exit2_4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_SignUP form = new Form_SignUP();
            this.Hide(); form.Show();
        }
        private void Exit2_5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_Search form = new Form_Search();
            this.Hide(); form.Show();
        }

        //Database demonstrarion
        private void AddDataGrid(Fields row) { dataGridView_InfoEdit.Rows.Add(row.Id, row.TournamentName, row.Place1, row.Place2, row.Place3, row.TournamentInfo); }
        private void HeaderOfTheTable()
        {
            var column1 = new DataGridViewColumn();
            column1.HeaderText = "Id";
            column1.Name = "id";
            column1.CellTemplate = new DataGridViewTextBoxCell();

            var column2 = new DataGridViewColumn();
            column2.HeaderText = "Назва турніру";
            column2.Name = "tournamentName";
            column2.CellTemplate = new DataGridViewTextBoxCell();

            var column3 = new DataGridViewColumn();
            column3.HeaderText = "1 місце";
            column3.Name = "1st place";
            column3.CellTemplate = new DataGridViewTextBoxCell();

            var column4 = new DataGridViewColumn();
            column4.HeaderText = "2 місце";
            column4.Name = "2nd place";
            column4.CellTemplate = new DataGridViewTextBoxCell();

            var column5 = new DataGridViewColumn();
            column5.HeaderText = "3 місце";
            column5.Name = "3rd place";
            column5.CellTemplate = new DataGridViewTextBoxCell();

            var column6 = new DataGridViewColumn();
            column6.HeaderText = "Інформація про турнір";
            column6.Name = "tournamentInfo";
            column6.CellTemplate = new DataGridViewTextBoxCell();

            dataGridView_InfoEdit.Columns.Add(column1);
            dataGridView_InfoEdit.Columns.Add(column2);
            dataGridView_InfoEdit.Columns.Add(column3);
            dataGridView_InfoEdit.Columns.Add(column4);
            dataGridView_InfoEdit.Columns.Add(column5);
            dataGridView_InfoEdit.Columns.Add(column6);
            dataGridView_InfoEdit.AllowUserToAddRows = false;
        }
        private void Form_InfoEdit_Shown(object sender, EventArgs e)
        {
            user = new User();
            HeaderOfTheTable();
            dataGridView_InfoEdit.Columns[0].ReadOnly = true;
            List<Fields> _data = new List<Fields>();
            DataBase _mySqlConnection = new DataBase();
            MySqlCommand _mySqlCommand = new MySqlCommand("SELECT * FROM `tournament`", _mySqlConnection.GetConnection());
            MySqlDataReader _reader;
            try
            {
                _mySqlConnection.OpenConnection();
                _reader = _mySqlCommand.ExecuteReader();
                while (_reader.Read())
                {
                    Fields row = new Fields(_reader["id"], _reader["tournamentName"], _reader["1st place"], _reader["2nd place"], _reader["3rd place"], _reader["tournamentInfo"]);
                    _data.Add(row);
                }
                for (int i = 0; i < _data.Count; i++) { AddDataGrid(_data[i]); }
            }
            catch { MessageBox.Show("Щось пішло не так", "Несподіванка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            finally
            {
                dataGridView_InfoEdit.AllowUserToResizeColumns = true;
                dataGridView_InfoEdit.AllowUserToResizeRows = true;
            }
        }

        //NumericUpDown + Buttons for them
        private void button_InfoEdit_Add_Click(object sender, EventArgs e)
        {
            dataGridView_InfoEdit.DataSource = null;
            dataGridView_InfoEdit.Rows.Clear();
            dataGridView_InfoEdit.RowCount = Convert.ToInt32(numericUpDown_InfoEdit_Add.Value);
            dataGridView_InfoEdit.ReadOnly = false;
            for (int i = 0; i < dataGridView_InfoEdit.Rows.Count; i++) { dataGridView_InfoEdit.Rows[i].Cells[0].ReadOnly = true; }
        }
        private void button_InfoEdit_Find_Click(object sender, EventArgs e)
        {
            dataGridView_InfoEdit.DataSource = null;
            dataGridView_InfoEdit.Rows.Clear();
            _data.Clear();
            DataBase _mySqlConnection = new DataBase();
            MySqlCommand _mySqlCommand = new MySqlCommand("SELECT * FROM `tournament`", _mySqlConnection.GetConnection());
            MySqlDataReader _reader;
            try
            {
                _mySqlConnection.OpenConnection();
                _reader = _mySqlCommand.ExecuteReader();
                while (_reader.Read())
                {
                    Fields row = new Fields(_reader["id"], _reader["tournamentName"], _reader["1st place"], _reader["2nd place"], _reader["3rd place"], _reader["tournamentInfo"]);
                    _data.Add(row);
                }
                int i = Convert.ToInt32(numericUpDown_InfoEdit_Select.Value) - 1;
                if (i >= 0 && i < _data.Count)
                { AddDataGrid(_data[i]); }
                else { MessageBox.Show("Ви вибрали неправильний елемент!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
            catch { MessageBox.Show("Помилка роботи з базою даних!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            finally { _mySqlConnection.CloseConnection(); }
        }
        
        //ToolStrip - Refresh, Search, Edit, Delete
        private void Refresh2_1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _data.Clear();
            DataBase _mySqlConnection = new DataBase();
            MySqlCommand _mySqlCommand = new MySqlCommand("SELECT * FROM `tournament`", _mySqlConnection.GetConnection());
            MySqlDataReader _reader;
            dataGridView_InfoEdit.DataSource = null;
            dataGridView_InfoEdit.Rows.Clear();
            try
            {
                _mySqlConnection.OpenConnection();
                _reader = _mySqlCommand.ExecuteReader();
                while (_reader.Read())
                {
                    Fields row = new Fields(_reader["id"], _reader["tournamentName"], _reader["1st place"], _reader["2nd place"], _reader["3rd place"], _reader["tournamentInfo"]);
                    _data.Add(row);
                }
                for (int i = 0; i < _data.Count; i++)
                {
                    AddDataGrid(_data[i]);
                    dataGridView_InfoEdit.Rows[i].Cells[0].ReadOnly = true;
                }
            }
            catch { MessageBox.Show("Помилка роботи з базою даних!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error);  }
            finally { _mySqlConnection.CloseConnection(); }
        }
        private void Refresh2_2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Додати ці дані в базу даних?", "Увага!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                bool add = true;
                DataBase _mySqlConnection = new DataBase();
                try
                {
                    _mySqlConnection.OpenConnection();
                    for (int i = 0; i < dataGridView_InfoEdit.Rows.Count; i++)
                    {
                        if (Convert.ToString(this.dataGridView_InfoEdit.Rows[i].Cells[1].Value) != "" &&
                            Convert.ToString(this.dataGridView_InfoEdit.Rows[i].Cells[2].Value) != "" &&
                            Convert.ToString(this.dataGridView_InfoEdit.Rows[i].Cells[3].Value) != "" &&
                            Convert.ToString(this.dataGridView_InfoEdit.Rows[i].Cells[4].Value) != "" &&
                            Convert.ToString(this.dataGridView_InfoEdit.Rows[i].Cells[5].Value) != "")
                        {
                            string _commandString = "INSERT INTO `tournament` (`tournamentName`, `1st place`, `2nd place`, `3rd place`, `tournamentInfo`)" + "VALUES(@tN, @1P, @2P, @3P, @tI)";
                            MySqlCommand _mySqlCommand = new MySqlCommand(_commandString, _mySqlConnection.GetConnection());
                            _mySqlCommand.Parameters.Add("@tN", MySqlDbType.VarChar).Value = this.dataGridView_InfoEdit.Rows[i].Cells[1].Value;
                            _mySqlCommand.Parameters.Add("@1P", MySqlDbType.VarChar).Value = this.dataGridView_InfoEdit.Rows[i].Cells[2].Value;
                            _mySqlCommand.Parameters.Add("@2P", MySqlDbType.VarChar).Value = this.dataGridView_InfoEdit.Rows[i].Cells[3].Value;
                            _mySqlCommand.Parameters.Add("@3P", MySqlDbType.VarChar).Value = this.dataGridView_InfoEdit.Rows[i].Cells[4].Value;
                            _mySqlCommand.Parameters.Add("@tI", MySqlDbType.VarChar).Value = this.dataGridView_InfoEdit.Rows[i].Cells[5].Value;
                            if (_mySqlCommand.ExecuteNonQuery() != 1) { add = false; }
                        }
                        else if (Convert.ToString(this.dataGridView_InfoEdit.Rows[i].Cells[1].Value) == "" ||
                            Convert.ToString(this.dataGridView_InfoEdit.Rows[i].Cells[2].Value) == "" ||
                            Convert.ToString(this.dataGridView_InfoEdit.Rows[i].Cells[3].Value) == "" ||
                            Convert.ToString(this.dataGridView_InfoEdit.Rows[i].Cells[4].Value) == "" ||
                            Convert.ToString(this.dataGridView_InfoEdit.Rows[i].Cells[5].Value) == "")
                        { MessageBox.Show("Не всі поля заповнені!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    if (add)  { MessageBox.Show("Дані добавлені!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Information); }
                    else { MessageBox.Show("Помилка добавлення даних!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                }
                catch { MessageBox.Show("Помилка роботи з базою даних!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                finally { _mySqlConnection.CloseConnection(); }
            }
        }
        private void Edit2_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Ви точно хочете змінити дані?", "Увага!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (dataGridView_InfoEdit.SelectedRows.Count == 0)
                {
                    if (Convert.ToString(this.dataGridView_InfoEdit.Rows[0].Cells[1].Value) != "" &&
                       Convert.ToString(this.dataGridView_InfoEdit.Rows[0].Cells[2].Value) != "" &&
                       Convert.ToString(this.dataGridView_InfoEdit.Rows[0].Cells[3].Value) != "" &&
                       Convert.ToString(this.dataGridView_InfoEdit.Rows[0].Cells[4].Value) != "" &&
                       Convert.ToString(this.dataGridView_InfoEdit.Rows[0].Cells[5].Value) != "")
                    {
                        string id = Convert.ToString(this.dataGridView_InfoEdit.Rows[0].Cells[0].Value);
                        string tournamentName = Convert.ToString(this.dataGridView_InfoEdit.Rows[0].Cells[1].Value);
                        string place1 = Convert.ToString(this.dataGridView_InfoEdit.Rows[0].Cells[2].Value);
                        string place2 = Convert.ToString(this.dataGridView_InfoEdit.Rows[0].Cells[3].Value);
                        string place3 = Convert.ToString(this.dataGridView_InfoEdit.Rows[0].Cells[4].Value);
                        string tournamentInfo = Convert.ToString(this.dataGridView_InfoEdit.Rows[0].Cells[5].Value);
                        DataBase _mySqlConnection = new DataBase();
                        string _commandString = "UPDATE `tournament` SET `id` = '" + id + "', " +
                            "`tournamentName` = '" + tournamentName + "' , " +
                            "`1st place` = '" + place1 + "' , " +
                            "`2nd place` = '" + place2 + "' , " +
                            "`3rd place` = '" + place3 + "' , " +
                            "`tournamentInfo` = '" + tournamentInfo + "' " +
                            "WHERE `tournament` . `id` = " + id;
                        MySqlCommand _mySqlCommand = new MySqlCommand(_commandString, _mySqlConnection.GetConnection());
                        try
                        {
                            _mySqlConnection.OpenConnection();
                            _mySqlCommand.ExecuteNonQuery();
                            MessageBox.Show("Дані були змінені!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        catch { MessageBox.Show("Помилка роботи з базою даних!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                        finally { _mySqlConnection.CloseConnection(); }
                    }
                    else { MessageBox.Show("Не всі поля заповнені!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                }
                else
                {
                    DataBase _mySqlConnection = new DataBase();
                    _mySqlConnection.OpenConnection();
                    bool changed = true;
                    for (int i = 0; i < dataGridView_InfoEdit.SelectedRows.Count; i++)
                    {
                        if (Convert.ToString(this.dataGridView_InfoEdit.SelectedRows[i].Cells[1].Value) != "" &&
                        Convert.ToString(this.dataGridView_InfoEdit.SelectedRows[i].Cells[2].Value) != "" &&
                       Convert.ToString(this.dataGridView_InfoEdit.SelectedRows[i].Cells[3].Value) != "" &&
                       Convert.ToString(this.dataGridView_InfoEdit.SelectedRows[i].Cells[4].Value) != "" &&
                       Convert.ToString(this.dataGridView_InfoEdit.SelectedRows[i].Cells[5].Value) != "")
                        {
                            string id = Convert.ToString(this.dataGridView_InfoEdit.SelectedRows[0].Cells[0].Value);
                            string tournamentName = Convert.ToString(this.dataGridView_InfoEdit.SelectedRows[0].Cells[1].Value);
                            string place1 = Convert.ToString(this.dataGridView_InfoEdit.SelectedRows[0].Cells[2].Value);
                            string place2 = Convert.ToString(this.dataGridView_InfoEdit.SelectedRows[0].Cells[3].Value);
                            string place3 = Convert.ToString(this.dataGridView_InfoEdit.SelectedRows[0].Cells[4].Value);
                            string tournamentInfo = Convert.ToString(this.dataGridView_InfoEdit.SelectedRows[0].Cells[5].Value);
                            string _commandString = "UPDATE `tournament` SET `id` = '" + id + "', " +
                            "`tournamentName` = '" + tournamentName + "' , " +
                            "`1st place` = '" + place1 + "' , " +
                            "`2nd place` = '" + place2 + "' , " +
                            "`3rd place` = '" + place3 + "' , " +
                            "`tournamentInfo` = '" + tournamentInfo + "' " +
                            "WHERE `tournament` . `id` = " + id;
                            MySqlCommand _mySqlCommand = new MySqlCommand(_commandString, _mySqlConnection.GetConnection());
                            try
                            {
                                if (_mySqlCommand.ExecuteNonQuery() != 1)
                                    changed = false;
                            }
                            catch { MessageBox.Show("Помилка роботи з базою даних!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                        }
                        else { MessageBox.Show("Не всі поля заповнені!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    if (changed) { MessageBox.Show("Дані були змінені!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Information); }
                    else { MessageBox.Show("Не всі дані були змінені!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
                    _mySqlConnection.CloseConnection();
                }
            }
        }
        private void Delete2_1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Видалити всі дані?", "Увага!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                DataBase _mySqlConnection = new DataBase();
                MySqlCommand _mySqlCommand = new MySqlCommand("TRUNCATE TABLE `tournament`", _mySqlConnection.GetConnection());
                try
                {
                    _mySqlConnection.OpenConnection();
                    _mySqlCommand.ExecuteNonQuery();
                    MessageBox.Show("Дані були видалені!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch { MessageBox.Show("Помилка видалення даних!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                finally { _mySqlConnection.CloseConnection(); }
            }
        }
        private void Delete2_2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Видалити ці дані?", "Увага!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (dataGridView_InfoEdit.SelectedRows.Count == 0)
                {
                    int index = Convert.ToInt32(numericUpDown_InfoEdit_Select.Value);
                    if (index > 0 && index <= _data.Count)
                    {
                        DataBase _mySqlConnection = new DataBase();
                        string id = Convert.ToString(this.dataGridView_InfoEdit.Rows[0].Cells[0].Value);
                        string _commandString = "DELETE FROM `tournament` WHERE `tournament` . `id` = " + id;
                        MySqlCommand _mySqlCommand = new MySqlCommand(_commandString, _mySqlConnection.GetConnection());
                        try
                        {
                            _mySqlConnection.OpenConnection();
                            _mySqlCommand.ExecuteNonQuery();
                            MessageBox.Show("Дані були видалені!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        catch { MessageBox.Show("Помилка роботи з базою даних!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                        finally {_mySqlConnection.CloseConnection(); }
                    }
                    else { MessageBox.Show("Вибирати необхідно весь рядок!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                }
                else
                {
                    DataBase _manager = new DataBase();
                    _manager.OpenConnection();
                    bool delete = true;
                    foreach (DataGridViewRow row in dataGridView_InfoEdit.SelectedRows)
                    {
                        string id = Convert.ToString(row.Cells[0].Value);
                        string _commandString = "DELETE FROM `tournament` WHERE `tournament`. `id` = " + id;
                        MySqlCommand _command = new MySqlCommand(_commandString, _manager.GetConnection());
                        try
                        {
                            dataGridView_InfoEdit.Rows.Remove(row);
                            if (_command.ExecuteNonQuery() != 1)
                                delete = false;
                        }
                        catch { MessageBox.Show("Помилка роботи з базою даних!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    if (delete) { MessageBox.Show("Дані були видалені!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
                    else
                    { MessageBox.Show("Не всі дані були видалені!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Error);  }
                    _manager.CloseConnection();
                }
            }
        }
    }
}
