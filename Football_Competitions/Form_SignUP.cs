﻿using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Football_Competitions
{
    public partial class Form_SignUP : Form
    {

        public Form_SignUP()
        {
            InitializeComponent();
        }
        //Перевірка (1.Чи зайнятий логін? 2.Чи існує такий користувач? 3.Pattern)
        private bool IsLogin
        {
            get
            {
                bool busy = false;
                string loginUser = textBox_SignUP_Login.Text;
                DataBase _mySQLConnection = new DataBase();
                DataTable _dataTable = new DataTable();
                MySqlDataAdapter _mySqlDataAdapter = new MySqlDataAdapter();
                MySqlCommand _mySqlCommand = new MySqlCommand("SELECT * FROM `sign(in/up)` WHERE `login` = @uL", _mySQLConnection.GetConnection());
                _mySqlCommand.Parameters.Add("@uL", MySqlDbType.VarChar).Value = loginUser;
                _mySqlDataAdapter.SelectCommand = _mySqlCommand;
                _mySqlDataAdapter.Fill(_dataTable);
                if (_dataTable.Rows.Count > 0)
                { busy = true; }
                else { busy = false; }
                return busy;
            }
        }
        private bool IsUser
        {
            get
            {
                bool busy = false;
                string loginUser = textBox_SignUP_Login.Text;
                string nameUser = textBox_SignUP_Name.Text;
                string surnameUser = textBox_SignUP_Surname.Text;
                string passwordUser = textBox_SignUP_Password.Text;
                DataBase _mySQLConnection = new DataBase();
                DataTable _dataTable = new DataTable();
                MySqlDataAdapter _mySqlDataAdapter = new MySqlDataAdapter();
                MySqlCommand _mySqlCommand = new MySqlCommand("SELECT * FROM `sign(in/up)` WHERE `login` = @uL AND `name` = @uN AND `surname` = @uS AND `password` = @uP", _mySQLConnection.GetConnection());
                _mySqlCommand.Parameters.Add("@uL", MySqlDbType.VarChar).Value = loginUser;
                _mySqlCommand.Parameters.Add("@uN", MySqlDbType.VarChar).Value = nameUser;
                _mySqlCommand.Parameters.Add("@uS", MySqlDbType.VarChar).Value = surnameUser;
                _mySqlCommand.Parameters.Add("@uP", MySqlDbType.VarChar).Value = passwordUser;
                _mySqlDataAdapter.SelectCommand = _mySqlCommand;
                _mySqlDataAdapter.Fill(_dataTable);
                if (_dataTable.Rows.Count > 0)
                { busy = true; }
                else { busy = false; }
                return busy;
            }
        }
        private bool IsPattern 
        {
            get
            {
                bool cool = false;
                Regex regexNameSurname = new Regex(@"^[^0-9ЪЫИЁёа-яґєіїa-z][^0-9]+$");
                Regex regexLogin = new Regex(@"[\wA-Z_\d]{1,15}");
                Regex regexPassword = new Regex(@"[\w\dA-Z]{1,16}");
                string NameCheck = textBox_SignUP_Name.Text;
                string SurnameCheck = textBox_SignUP_Surname.Text;
                string LoginCheck = textBox_SignUP_Login.Text;
                string PasswordCheck = textBox_SignUP_Password.Text;
                if (regexNameSurname.IsMatch(NameCheck) && regexNameSurname.IsMatch(SurnameCheck) && regexLogin.IsMatch(LoginCheck) && regexPassword.IsMatch(PasswordCheck))
                { cool = true; }
                else { cool = false; }
                return cool;
            }
        }

        private void Form_SignUP_Load(object sender, EventArgs e)
        {
            //Placeholders
            textBox_SignUP_Name.Text = "Введіть ваше ім'я!";
            textBox_SignUP_Name.ForeColor = Color.Gray;
            textBox_SignUP_Surname.Text = "Введіть прізвище!";
            textBox_SignUP_Surname.ForeColor = Color.Gray;
            textBox_SignUP_Login.Text = "Введіть логін!";
            textBox_SignUP_Login.ForeColor = Color.Gray;
            textBox_SignUP_Password.Text = "Введіть пароль!";
            textBox_SignUP_Password.ForeColor = Color.Gray;
        }
        //Обробники подій стосовно placeholders
        private void textBox_SignUP_Name_Enter(object sender, EventArgs e)
        { if (textBox_SignUP_Name.Text == "Введіть ваше ім'я!") { textBox_SignUP_Name.Text = ""; textBox_SignUP_Name.ForeColor = Color.Black; } }
        private void textBox_SignUP_Name_Leave(object sender, EventArgs e)
        { if (textBox_SignUP_Name.Text == "") { textBox_SignUP_Name.Text = "Введіть ваше ім'я!"; textBox_SignUP_Name.ForeColor = Color.Gray; } }
        private void textBox_SignUP_Surname_Enter(object sender, EventArgs e)
        { if (textBox_SignUP_Surname.Text == "Введіть прізвище!") { textBox_SignUP_Surname.Text = ""; textBox_SignUP_Surname.ForeColor = Color.Black; } }
        private void textBox_SignUP_Surname_Leave(object sender, EventArgs e)
        { if (textBox_SignUP_Surname.Text == "") { textBox_SignUP_Surname.Text = "Введіть прізвище!"; textBox_SignUP_Surname.ForeColor = Color.Gray; } }
        private void textBox_SignUP_Login_Enter(object sender, EventArgs e)
        { if (textBox_SignUP_Login.Text == "Введіть логін!") { textBox_SignUP_Login.Text = ""; textBox_SignUP_Login.ForeColor = Color.Black; } }
        private void textBox_SignUP_Login_Leave(object sender, EventArgs e)
        { if (textBox_SignUP_Login.Text == "") { textBox_SignUP_Login.Text = "Введіть логін!"; textBox_SignUP_Login.ForeColor = Color.Gray; } }
        private void textBox_SignUP_Password_Enter(object sender, EventArgs e)
        { if (textBox_SignUP_Password.Text == "Введіть пароль!") { textBox_SignUP_Password.Text = ""; textBox_SignUP_Password.ForeColor = Color.Black; } }
        private void textBox_SignUP_Password_Leave(object sender, EventArgs e)
        { if (textBox_SignUP_Password.Text == "") { textBox_SignUP_Password.Text = "Введіть пароль!"; textBox_SignUP_Password.ForeColor = Color.Gray; } }

        //Події за кліком
        private void linkLabel_SignUP_SignIN_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Form_SignIN form = new Form_SignIN();
            this.Hide(); form.Show();
        }
        private void button_SignUP_SignUP_Click(object sender, EventArgs e)
        {
            if ((textBox_SignUP_Name.Text == "Введіть ваше ім'я!" || textBox_SignUP_Surname.Text == "Введіть ваше прізвище!" || textBox_SignUP_Login.Text == "Введіть логін!" || textBox_SignUP_Password.Text == "Введіть пароль!"))
            {
                MessageBox.Show("Необхідно ввести усі поля!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!IsPattern)
            {
                string patternInfo = "Щось з введеного не прошло перевірку дозволеного вводу.\n\n" + "\t\tПам'ятайте!\nІм'я та прізвище не містять цифр та починаються з великої літери\n" 
                    + "Вводьте тільки цифри, букви або символ _ (в логіні).";
                MessageBox.Show(patternInfo, "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (!IsUser)
            {
                if (!IsLogin)
                {
                    DataBase _mySQLConnection = new DataBase();
                    MySqlCommand _mySqlCommand = new MySqlCommand("INSERT INTO `sign(in/up)`( `login`, `password` , `name` , `surname`, `admin`) " +
                        "VALUES (@login, @password, @name, @surname, @admin)", _mySQLConnection.GetConnection());
                    try
                    {
                        _mySqlCommand.Parameters.Add("@login", MySqlDbType.VarChar).Value = textBox_SignUP_Login.Text;
                        _mySqlCommand.Parameters.Add("@password", MySqlDbType.VarChar).Value = textBox_SignUP_Password.Text;
                        _mySqlCommand.Parameters.Add("@name", MySqlDbType.VarChar).Value = textBox_SignUP_Name.Text;
                        _mySqlCommand.Parameters.Add("@surname", MySqlDbType.VarChar).Value = textBox_SignUP_Surname.Text;
                        _mySqlCommand.Parameters.Add("@admin", MySqlDbType.VarChar).Value = "false";
                        _mySQLConnection.OpenConnection();
                        if (_mySqlCommand.ExecuteNonQuery() == 1)
                        {
                            MessageBox.Show("Аккаунт створено!", "Успіх!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Form_InfoShow form = new Form_InfoShow();
                            this.Hide();
                            form.Show();
                            User user = new User(textBox_SignUP_Login.Text);
                        }
                        else { MessageBox.Show("Помилка створення аккаунту!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    }
                    catch { MessageBox.Show("Помилка роботи з базою даних!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    finally { _mySQLConnection.CloseConnection(); }
                }
                else { MessageBox.Show("Логін вже зайнято!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
            }
            else 
            {
                if (MessageBox.Show("Такий користувач вже існує!\nПерейти на вкладку входу?", "Несподіванка!", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    Form_SignIN form = new Form_SignIN();
                    this.Hide(); form.Show();
                }
            }
        }
    }
}


