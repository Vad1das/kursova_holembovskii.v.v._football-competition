﻿CREATE TABLE [dbo].[Tournament] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (50) NULL,
    [1st place] NVARCHAR (50) NULL,
    [2nd place] NVARCHAR (50) NULL,
    [3nd place] NVARCHAR (50) NULL,
    [Info]      NVARCHAR(MAX)          NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

