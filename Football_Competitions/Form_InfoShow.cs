﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Football_Competitions
{
    public partial class Form_InfoShow : Form
    {
        private User user;
        private List<Fields> _data = new List<Fields>();
        public Form_InfoShow()
        {
            InitializeComponent(); 
        }

        //Database demonstrarion
        private void AddDataGrid(Fields row) { dataGridView_InfoShow.Rows.Add(row.Id, row.TournamentName, row.Place1, row.Place2, row.Place3, row.TournamentInfo); }
        private void HeaderOfTheTable()
        {
            var column1 = new DataGridViewColumn();
            column1.HeaderText = "Id";
            column1.Name = "id";
            column1.CellTemplate = new DataGridViewTextBoxCell();

            var column2 = new DataGridViewColumn();
            column2.HeaderText = "Назва турніру";
            column2.Name = "tournamentName";
            column2.CellTemplate = new DataGridViewTextBoxCell();

            var column3 = new DataGridViewColumn();
            column3.HeaderText = "1 місце";
            column3.Name = "1st place";
            column3.CellTemplate = new DataGridViewTextBoxCell();

            var column4 = new DataGridViewColumn();
            column4.HeaderText = "2 місце";
            column4.Name = "2nd place";
            column4.CellTemplate = new DataGridViewTextBoxCell();

            var column5 = new DataGridViewColumn();
            column5.HeaderText = "3 місце";
            column5.Name = "3rd place";
            column5.CellTemplate = new DataGridViewTextBoxCell();

            var column6 = new DataGridViewColumn();
            column6.HeaderText = "Інформація про турнір";
            column6.Name = "tournamentInfo";
            column6.CellTemplate = new DataGridViewTextBoxCell();

            dataGridView_InfoShow.Columns.Add(column1);
            dataGridView_InfoShow.Columns.Add(column2);
            dataGridView_InfoShow.Columns.Add(column3);
            dataGridView_InfoShow.Columns.Add(column4);
            dataGridView_InfoShow.Columns.Add(column5);
            dataGridView_InfoShow.Columns.Add(column6);

            dataGridView_InfoShow.AllowUserToAddRows = false;
            dataGridView_InfoShow.ReadOnly = true;
        }
        private void Form_InfoShow_Shown(object sender, EventArgs e)
        {
            user = new User();
            HeaderOfTheTable();
            List<Fields> _data = new List<Fields>();
            DataBase _mySqlConnection = new DataBase();
            MySqlCommand _mySqlCommand = new MySqlCommand("SELECT * FROM `tournament`", _mySqlConnection.GetConnection());
            MySqlDataReader _reader;
            try
            {
                _mySqlConnection.OpenConnection();
                _reader = _mySqlCommand.ExecuteReader();
                while (_reader.Read())
                {
                    Fields row = new Fields(_reader["id"], _reader["tournamentName"], _reader["1st place"], _reader["2nd place"], _reader["3rd place"], _reader["tournamentInfo"]);
                    _data.Add(row);
                }
                for (int i = 0; i < _data.Count; i++) { AddDataGrid(_data[i]); }
            }
            catch { MessageBox.Show("Щось пішло не так", "Несподіванка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            finally
            {
                dataGridView_InfoShow.AllowUserToResizeColumns = true;
                dataGridView_InfoShow.AllowUserToResizeRows = true;
            }
        }

        //ToolStrip - Info, Help, Different exits, Admin mode, Refresh, Search
        private void Info1_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string infostring = "Інформаційно-пошукова система: футбольні змагання.\n" + "Звичайний користувач може переглядати, шукати інформацію. Адмін може її ще змінювати, видаляти.\n"
                + "Автор: Голембовський Вадим КІ-19-1[1].\n";
            MessageBox.Show(infostring, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void Help1_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string helpstring = "Це форма показу даних.\n" + "Невеличкі підказки.\n" + "Для пошуку запису за ID виберіть чи впишіть значення та натисність на кнопку Select.\n"
                + "Тоді база даних видасть вам тільки запис з цим ID.\n" + "Для того, щоб всі дані знову відображалися натисніть на Refresh.";
            MessageBox.Show(helpstring, "Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void Exit1_1ToolStripMenuItem_Click(object sender, EventArgs e) { Application.Exit(); }
        private void Exit1_2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_SignIN form = new Form_SignIN();
            this.Hide(); form.Show();
        }
        private void Exit1_3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_SignUP form = new Form_SignUP();
            this.Hide(); form.Show();
        }
        private void AdminMode1_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (user.userAdmin == "true")
            {
                Form_InfoEdit form = new Form_InfoEdit();
                this.Hide(); form.Show();
            }
            else { MessageBox.Show("Ви не адмін!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
        private void Refresh1_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<Fields> _data = new List<Fields>();
            DataBase _mySqlConnection = new DataBase();
            MySqlCommand _mySqlCommand = new MySqlCommand("SELECT * FROM `tournament`", _mySqlConnection.GetConnection());
            MySqlDataReader _reader;
            _mySqlConnection.OpenConnection();
            _reader = _mySqlCommand.ExecuteReader(); 
            dataGridView_InfoShow.DataSource = null;
            dataGridView_InfoShow.Rows.Clear();
            try
            {
                while (_reader.Read())
                {
                    Fields row = new Fields(_reader["id"], _reader["tournamentName"], _reader["1st place"], _reader["2nd place"], _reader["3rd place"], _reader["tournamentInfo"]);
                    _data.Add(row);
                }
                for (int i = 0; i < _data.Count; i++) { AddDataGrid(_data[i]); }
                MessageBox.Show("Дані обновлені!", "Увага!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch { MessageBox.Show("Помилка роботи з базою даних!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            finally { _mySqlConnection.CloseConnection(); }
        }
        private void Search1_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_Search form = new Form_Search();
            this.Hide(); form.Show();
        }

        //Search - ID by NumericUpDown
        private void button_InfoShow_Find_Click(object sender, EventArgs e)
        {
            dataGridView_InfoShow.DataSource = null;
            dataGridView_InfoShow.Rows.Clear();
            _data.Clear();
            DataBase _mySqlConnection = new DataBase();
            MySqlCommand _mySqlCommand = new MySqlCommand("SELECT * FROM `tournament`", _mySqlConnection.GetConnection());
            MySqlDataReader _reader;
            try
            {
                _mySqlConnection.OpenConnection();
                _reader = _mySqlCommand.ExecuteReader();
                while (_reader.Read())
                {
                    Fields row = new Fields(_reader["id"], _reader["tournamentName"], _reader["1st place"], _reader["2nd place"], _reader["3rd place"], _reader["tournamentInfo"]);
                    _data.Add(row);
                }
                int i = Convert.ToInt32(numericUpDown_InfoShow_Select.Value) - 1;
                if (i >= 0 && i < _data.Count)
                { AddDataGrid(_data[i]); }
                else { MessageBox.Show("Ви вибрали неправильний елемент!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
            catch { MessageBox.Show("Помилка роботи з базою даних!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            finally { _mySqlConnection.CloseConnection(); }
        }
    }
}
