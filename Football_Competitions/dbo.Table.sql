﻿CREATE TABLE [dbo].[Tournament]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NULL, 
    [1st place] NVARCHAR(50) NULL, 
    [2nd place] NVARCHAR(50) NULL, 
    [3nd place] NVARCHAR(50) NULL, 
    [Info] TEXT NULL
)
