﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Football_Competitions
{
    public partial class Form_SignIN : Form
    {
        string userAdmin;
        public Form_SignIN()
        {
            InitializeComponent();
        }
        private void Form_SignIN_Load(object sender, EventArgs e)
        {
            //Placeholders
            textBox_SignIN_Login.Text = "Введіть логін!";
            textBox_SignIN_Login.ForeColor = Color.Gray;
            textBox_SignIN_Password.Text = "Введіть пароль!";
            textBox_SignIN_Password.ForeColor = Color.Gray;
        }
        //Обробники подій стосовно placeholders
        private void textBox_SignIN_Login_Enter(object sender, EventArgs e)
        { if (textBox_SignIN_Login.Text == "Введіть логін!") { textBox_SignIN_Login.Text = ""; textBox_SignIN_Login.ForeColor = Color.Black; } }
        private void textBox_SignIN_Login_Leave(object sender, EventArgs e)
        { if (textBox_SignIN_Login.Text == "") { textBox_SignIN_Login.Text = "Введіть логін!"; textBox_SignIN_Login.ForeColor = Color.Gray; } }
        private void textBox_SignIN_Password_Enter(object sender, EventArgs e)
        { if (textBox_SignIN_Password.Text == "Введіть пароль!") { textBox_SignIN_Password.Text = ""; textBox_SignIN_Password.ForeColor = Color.Black; } }
        private void textBox_SignIN_Passeord_Leave(object sender, EventArgs e)
        { if (textBox_SignIN_Password.Text == "") { textBox_SignIN_Password.Text = "Введіть пароль!"; textBox_SignIN_Password.ForeColor = Color.Gray; } }

        public bool IsUser
        {
            get
            {
                bool busy = false;
                string loginUser = textBox_SignIN_Login.Text;
                DataBase _mySQLConnection = new DataBase();
                DataTable _dataTable = new DataTable();
                MySqlDataAdapter _mySqlDataAdapter = new MySqlDataAdapter();
                MySqlCommand _mySqlCommand = new MySqlCommand("SELECT * FROM `sign(in/up)` WHERE `login` = @uL", _mySQLConnection.GetConnection());
                _mySqlCommand.Parameters.Add("@uL", MySqlDbType.VarChar).Value = loginUser; 
                _mySqlDataAdapter.SelectCommand = _mySqlCommand;
                _mySqlDataAdapter.Fill(_dataTable);
                if (_dataTable.Rows.Count > 0) { busy = true; }
                else { busy = false; }
                return busy;
            }
        }
        private bool IsAdmin 
        {
            get
            {
                bool admin = false;
                string loginUser = textBox_SignIN_Login.Text;
                string passwordUser = textBox_SignIN_Password.Text;
                DataBase _manager = new DataBase();
                MySqlCommand _command = new MySqlCommand("SELECT * FROM `sign(in/up)` WHERE `login` = @uL AND `password` = @uP", _manager.GetConnection());
                _command.Parameters.Add("@uL", MySqlDbType.VarChar).Value = loginUser;
                _command.Parameters.Add("@uP", MySqlDbType.VarChar).Value = passwordUser;
                MySqlDataReader _reader;
                _manager.OpenConnection();
                _reader = _command.ExecuteReader();
                while (_reader.Read()) { userAdmin = (string)_reader["admin"]; }
                if ((string)_reader["admin"] == "true") { admin = true; }
                else { admin = false; }
                _manager.CloseConnection();
                return admin;
            }
        }
        
        //Події за кліком
        private void linkLabel_SignIN_SignUP_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Form_SignUP form = new Form_SignUP();
            this.Hide(); form.Show();
        }
        private void button_SignIN_SignIN_Click(object sender, EventArgs e)
        {
            if ((textBox_SignIN_Login.Text == "Введіть логін!" || textBox_SignIN_Password.Text == "Введіть пароль!"))
            {
                MessageBox.Show("Необхідно ввести усі поля!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string loginUser = textBox_SignIN_Login.Text;
            string passwordUser = textBox_SignIN_Password.Text;
            DataBase _mySQLConnection = new DataBase();
            DataTable _dataTable = new DataTable();
            MySqlDataAdapter _mySqlDataAdapter = new MySqlDataAdapter();
            MySqlCommand _mySqlCommand = new MySqlCommand("SELECT * FROM `sign(in/up)` WHERE `login` = @uL AND `password` = @uP", _mySQLConnection.GetConnection());
            try
            {
                _mySqlCommand.Parameters.Add("@uL", MySqlDbType.VarChar).Value = loginUser;
                _mySqlCommand.Parameters.Add("@uP", MySqlDbType.VarChar).Value = passwordUser;
                _mySQLConnection.OpenConnection();
                _mySqlDataAdapter.SelectCommand = _mySqlCommand;
                _mySqlDataAdapter.Fill(_dataTable);
                if (_dataTable.Rows.Count > 0)
                {
                    if (IsAdmin) { User user = new User("true"); }
                    else { User user = new User("false"); }
                    Form_InfoShow form = new Form_InfoShow();
                    this.Hide(); form.Show();
                }
                else
                {
                    if (IsUser) { MessageBox.Show("Пароль введено неправильно!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
                    else
                    {
                        if (MessageBox.Show("Даний аккаунт відсутній.\nХочете зареєструватись?", "Несподіванка!", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            Form_SignUP form = new Form_SignUP();
                            this.Hide(); form.Show();
                        }
                    }
                }
            }
            catch { MessageBox.Show("Помилка роботи з базою даних!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            finally { _mySQLConnection.CloseConnection(); }
        }

    }
}
