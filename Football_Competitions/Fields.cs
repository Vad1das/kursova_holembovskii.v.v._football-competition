﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Football_Competitions
{
    class Fields
    {
        // Fields with GetSet methods 
        public object Id { get; set; }
        public object TournamentName { get; set; }
        public object Place1 { get; set; }
        public object Place2 { get; set; }
        public object Place3 { get; set; }
        public object TournamentInfo { get; set; }
        // Constructors
        public Fields() { }
        public Fields(object id, object tournamentName, object place1, object place2, object place3, object tournamentInfo)
        {
            Id = id;
            TournamentName = tournamentName;
            Place1 = place1;
            Place2 = place2;
            Place3 = place3;
            TournamentInfo = tournamentInfo;
        }
        // Method
        public void DataData(object id, object tournamentName, object place1, object place2, object place3, object tournamentInfo)
        {
            Id = id;
            TournamentName = tournamentName;
            Place1 = place1;
            Place2 = place2;
            Place3 = place3;
            TournamentInfo = tournamentInfo;
        }   
    } 
}

