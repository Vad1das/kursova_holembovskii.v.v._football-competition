﻿namespace Football_Competitions
{
    partial class Form_Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Search));
            this.label_Search_Search1 = new System.Windows.Forms.Label();
            this.label_Search_Search2 = new System.Windows.Forms.Label();
            this.textBox_Search_Search = new System.Windows.Forms.TextBox();
            this.button_Search_Search = new System.Windows.Forms.Button();
            this.dataGridView_Search = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.Exit3_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit3_1_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit3_2_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit3_3_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Return3_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Return3_1_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Return3_2_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Info3_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Help3_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Search)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_Search_Search1
            // 
            this.label_Search_Search1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label_Search_Search1.AutoSize = true;
            this.label_Search_Search1.BackColor = System.Drawing.Color.Transparent;
            this.label_Search_Search1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Search_Search1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_Search_Search1.Location = new System.Drawing.Point(375, 45);
            this.label_Search_Search1.Name = "label_Search_Search1";
            this.label_Search_Search1.Size = new System.Drawing.Size(231, 55);
            this.label_Search_Search1.TabIndex = 2;
            this.label_Search_Search1.Text = "SEARCH";
            // 
            // label_Search_Search2
            // 
            this.label_Search_Search2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label_Search_Search2.AutoSize = true;
            this.label_Search_Search2.BackColor = System.Drawing.Color.Transparent;
            this.label_Search_Search2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Search_Search2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_Search_Search2.Location = new System.Drawing.Point(227, 110);
            this.label_Search_Search2.Name = "label_Search_Search2";
            this.label_Search_Search2.Size = new System.Drawing.Size(152, 20);
            this.label_Search_Search2.TabIndex = 14;
            this.label_Search_Search2.Text = "Write something:";
            // 
            // textBox_Search_Search
            // 
            this.textBox_Search_Search.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBox_Search_Search.Location = new System.Drawing.Point(385, 112);
            this.textBox_Search_Search.Name = "textBox_Search_Search";
            this.textBox_Search_Search.Size = new System.Drawing.Size(220, 20);
            this.textBox_Search_Search.TabIndex = 18;
            // 
            // button_Search_Search
            // 
            this.button_Search_Search.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button_Search_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button_Search_Search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Search_Search.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_Search_Search.Location = new System.Drawing.Point(611, 110);
            this.button_Search_Search.Name = "button_Search_Search";
            this.button_Search_Search.Size = new System.Drawing.Size(96, 22);
            this.button_Search_Search.TabIndex = 19;
            this.button_Search_Search.Text = "Search";
            this.button_Search_Search.UseVisualStyleBackColor = false;
            this.button_Search_Search.Click += new System.EventHandler(this.button_Search_Search_Click);
            // 
            // dataGridView_Search
            // 
            this.dataGridView_Search.AllowUserToAddRows = false;
            this.dataGridView_Search.AllowUserToDeleteRows = false;
            this.dataGridView_Search.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_Search.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Search.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_Search.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_Search.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_Search.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_Search.Location = new System.Drawing.Point(12, 149);
            this.dataGridView_Search.Name = "dataGridView_Search";
            this.dataGridView_Search.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_Search.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_Search.RowTemplate.ReadOnly = true;
            this.dataGridView_Search.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_Search.Size = new System.Drawing.Size(960, 600);
            this.dataGridView_Search.TabIndex = 20;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Exit3_ToolStripMenuItem,
            this.Return3_ToolStripMenuItem,
            this.Info3_ToolStripMenuItem,
            this.Help3_ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(984, 24);
            this.menuStrip1.TabIndex = 21;
            this.menuStrip1.Text = "menuStrip_Search";
            // 
            // Exit3_ToolStripMenuItem
            // 
            this.Exit3_ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Exit3_ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Exit3_1_ToolStripMenuItem,
            this.Exit3_2_ToolStripMenuItem,
            this.Exit3_3_ToolStripMenuItem});
            this.Exit3_ToolStripMenuItem.Name = "Exit3_ToolStripMenuItem";
            this.Exit3_ToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.Exit3_ToolStripMenuItem.Text = "Exit";
            // 
            // Exit3_1_ToolStripMenuItem
            // 
            this.Exit3_1_ToolStripMenuItem.Name = "Exit3_1_ToolStripMenuItem";
            this.Exit3_1_ToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.Exit3_1_ToolStripMenuItem.Text = "Вихід з програми";
            this.Exit3_1_ToolStripMenuItem.Click += new System.EventHandler(this.Exit3_1_ToolStripMenuItem_Click);
            // 
            // Exit3_2_ToolStripMenuItem
            // 
            this.Exit3_2_ToolStripMenuItem.Name = "Exit3_2_ToolStripMenuItem";
            this.Exit3_2_ToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.Exit3_2_ToolStripMenuItem.Text = "В меню входу";
            this.Exit3_2_ToolStripMenuItem.Click += new System.EventHandler(this.Exit3_2_ToolStripMenuItem_Click);
            // 
            // Exit3_3_ToolStripMenuItem
            // 
            this.Exit3_3_ToolStripMenuItem.Name = "Exit3_3_ToolStripMenuItem";
            this.Exit3_3_ToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.Exit3_3_ToolStripMenuItem.Text = "В меню реєстрації";
            this.Exit3_3_ToolStripMenuItem.Click += new System.EventHandler(this.Exit3_3_ToolStripMenuItem_Click);
            // 
            // Return3_ToolStripMenuItem
            // 
            this.Return3_ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Return3_ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Return3_1_ToolStripMenuItem,
            this.Return3_2_ToolStripMenuItem});
            this.Return3_ToolStripMenuItem.Name = "Return3_ToolStripMenuItem";
            this.Return3_ToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.Return3_ToolStripMenuItem.Text = "Return";
            // 
            // Return3_1_ToolStripMenuItem
            // 
            this.Return3_1_ToolStripMenuItem.Name = "Return3_1_ToolStripMenuItem";
            this.Return3_1_ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.Return3_1_ToolStripMenuItem.Text = "В меню показу";
            this.Return3_1_ToolStripMenuItem.Click += new System.EventHandler(this.Return3_1_ToolStripMenuItem_Click);
            // 
            // Return3_2_ToolStripMenuItem
            // 
            this.Return3_2_ToolStripMenuItem.Name = "Return3_2_ToolStripMenuItem";
            this.Return3_2_ToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.Return3_2_ToolStripMenuItem.Text = "В меню редагування";
            this.Return3_2_ToolStripMenuItem.Click += new System.EventHandler(this.Return3_2_ToolStripMenuItem_Click);
            // 
            // Info3_ToolStripMenuItem
            // 
            this.Info3_ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Info3_ToolStripMenuItem.Name = "Info3_ToolStripMenuItem";
            this.Info3_ToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Info3_ToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.Info3_ToolStripMenuItem.Text = "Info";
            this.Info3_ToolStripMenuItem.Click += new System.EventHandler(this.Info3_ToolStripMenuItem_Click);
            // 
            // Help3_ToolStripMenuItem
            // 
            this.Help3_ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Help3_ToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Help3_ToolStripMenuItem.Name = "Help3_ToolStripMenuItem";
            this.Help3_ToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.Help3_ToolStripMenuItem.Text = "Help";
            this.Help3_ToolStripMenuItem.Click += new System.EventHandler(this.Help3_ToolStripMenuItem_Click);
            // 
            // Form_Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Football_Competitions.Properties.Resources.Background_Grass;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(984, 761);
            this.Controls.Add(this.dataGridView_Search);
            this.Controls.Add(this.button_Search_Search);
            this.Controls.Add(this.textBox_Search_Search);
            this.Controls.Add(this.label_Search_Search2);
            this.Controls.Add(this.label_Search_Search1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(800, 700);
            this.Name = "Form_Search";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Football - Пошук";
            this.Load += new System.EventHandler(this.Form_Search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Search)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Search_Search1;
        private System.Windows.Forms.Label label_Search_Search2;
        private System.Windows.Forms.TextBox textBox_Search_Search;
        private System.Windows.Forms.Button button_Search_Search;
        private System.Windows.Forms.DataGridView dataGridView_Search;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Info3_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Return3_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Return3_1_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Return3_2_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit3_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit3_1_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit3_2_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit3_3_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Help3_ToolStripMenuItem;
    }
}