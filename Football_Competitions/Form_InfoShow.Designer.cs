﻿namespace Football_Competitions
{
    partial class Form_InfoShow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_InfoShow));
            this.menuStrip_InfoShow = new System.Windows.Forms.MenuStrip();
            this.Refresh1_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit1_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit1_1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit1_2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit1_3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Info1_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Search1_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AdminMode1_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Help1_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label_InfoShow_Information = new System.Windows.Forms.Label();
            this.dataGridView_InfoShow = new System.Windows.Forms.DataGridView();
            this.label_InfoShow_Select = new System.Windows.Forms.Label();
            this.numericUpDown_InfoShow_Select = new System.Windows.Forms.NumericUpDown();
            this.button_InfoShow_Find = new System.Windows.Forms.Button();
            this.menuStrip_InfoShow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_InfoShow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_InfoShow_Select)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip_InfoShow
            // 
            this.menuStrip_InfoShow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.menuStrip_InfoShow.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Refresh1_ToolStripMenuItem,
            this.Exit1_ToolStripMenuItem,
            this.Info1_ToolStripMenuItem,
            this.Search1_ToolStripMenuItem,
            this.AdminMode1_ToolStripMenuItem,
            this.Help1_ToolStripMenuItem});
            this.menuStrip_InfoShow.Location = new System.Drawing.Point(0, 0);
            this.menuStrip_InfoShow.Name = "menuStrip_InfoShow";
            this.menuStrip_InfoShow.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip_InfoShow.Size = new System.Drawing.Size(984, 24);
            this.menuStrip_InfoShow.TabIndex = 0;
            this.menuStrip_InfoShow.Text = "menuStrip_InfoShow";
            // 
            // Refresh1_ToolStripMenuItem
            // 
            this.Refresh1_ToolStripMenuItem.Name = "Refresh1_ToolStripMenuItem";
            this.Refresh1_ToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.Refresh1_ToolStripMenuItem.Text = "Refresh";
            this.Refresh1_ToolStripMenuItem.Click += new System.EventHandler(this.Refresh1_ToolStripMenuItem_Click);
            // 
            // Exit1_ToolStripMenuItem
            // 
            this.Exit1_ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Exit1_ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Exit1_1ToolStripMenuItem,
            this.Exit1_2ToolStripMenuItem,
            this.Exit1_3ToolStripMenuItem});
            this.Exit1_ToolStripMenuItem.Name = "Exit1_ToolStripMenuItem";
            this.Exit1_ToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.Exit1_ToolStripMenuItem.Text = "Exit";
            // 
            // Exit1_1ToolStripMenuItem
            // 
            this.Exit1_1ToolStripMenuItem.Name = "Exit1_1ToolStripMenuItem";
            this.Exit1_1ToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.Exit1_1ToolStripMenuItem.Text = "Вийти з програми";
            this.Exit1_1ToolStripMenuItem.Click += new System.EventHandler(this.Exit1_1ToolStripMenuItem_Click);
            // 
            // Exit1_2ToolStripMenuItem
            // 
            this.Exit1_2ToolStripMenuItem.Name = "Exit1_2ToolStripMenuItem";
            this.Exit1_2ToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.Exit1_2ToolStripMenuItem.Text = "В меню входу";
            this.Exit1_2ToolStripMenuItem.Click += new System.EventHandler(this.Exit1_2ToolStripMenuItem_Click);
            // 
            // Exit1_3ToolStripMenuItem
            // 
            this.Exit1_3ToolStripMenuItem.Name = "Exit1_3ToolStripMenuItem";
            this.Exit1_3ToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.Exit1_3ToolStripMenuItem.Text = "В меню реєстрації";
            this.Exit1_3ToolStripMenuItem.Click += new System.EventHandler(this.Exit1_3ToolStripMenuItem_Click);
            // 
            // Info1_ToolStripMenuItem
            // 
            this.Info1_ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Info1_ToolStripMenuItem.Name = "Info1_ToolStripMenuItem";
            this.Info1_ToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Info1_ToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.Info1_ToolStripMenuItem.Text = "Info";
            this.Info1_ToolStripMenuItem.Click += new System.EventHandler(this.Info1_ToolStripMenuItem_Click);
            // 
            // Search1_ToolStripMenuItem
            // 
            this.Search1_ToolStripMenuItem.Name = "Search1_ToolStripMenuItem";
            this.Search1_ToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.Search1_ToolStripMenuItem.Text = "Search";
            this.Search1_ToolStripMenuItem.Click += new System.EventHandler(this.Search1_ToolStripMenuItem_Click);
            // 
            // AdminMode1_ToolStripMenuItem
            // 
            this.AdminMode1_ToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.AdminMode1_ToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.AdminMode1_ToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AdminMode1_ToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.AdminMode1_ToolStripMenuItem.Name = "AdminMode1_ToolStripMenuItem";
            this.AdminMode1_ToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.AdminMode1_ToolStripMenuItem.Text = "Admin mode";
            this.AdminMode1_ToolStripMenuItem.Click += new System.EventHandler(this.AdminMode1_ToolStripMenuItem_Click);
            // 
            // Help1_ToolStripMenuItem
            // 
            this.Help1_ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Help1_ToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Help1_ToolStripMenuItem.Name = "Help1_ToolStripMenuItem";
            this.Help1_ToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.Help1_ToolStripMenuItem.Text = "Help";
            this.Help1_ToolStripMenuItem.Click += new System.EventHandler(this.Help1_ToolStripMenuItem_Click);
            // 
            // label_InfoShow_Information
            // 
            this.label_InfoShow_Information.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label_InfoShow_Information.AutoSize = true;
            this.label_InfoShow_Information.BackColor = System.Drawing.Color.Transparent;
            this.label_InfoShow_Information.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_InfoShow_Information.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_InfoShow_Information.Location = new System.Drawing.Point(305, 45);
            this.label_InfoShow_Information.Name = "label_InfoShow_Information";
            this.label_InfoShow_Information.Size = new System.Drawing.Size(370, 55);
            this.label_InfoShow_Information.TabIndex = 1;
            this.label_InfoShow_Information.Text = "INFORMATION";
            // 
            // dataGridView_InfoShow
            // 
            this.dataGridView_InfoShow.AllowUserToAddRows = false;
            this.dataGridView_InfoShow.AllowUserToDeleteRows = false;
            this.dataGridView_InfoShow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_InfoShow.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_InfoShow.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_InfoShow.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_InfoShow.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_InfoShow.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_InfoShow.Location = new System.Drawing.Point(12, 156);
            this.dataGridView_InfoShow.Name = "dataGridView_InfoShow";
            this.dataGridView_InfoShow.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_InfoShow.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_InfoShow.RowTemplate.ReadOnly = true;
            this.dataGridView_InfoShow.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_InfoShow.Size = new System.Drawing.Size(960, 593);
            this.dataGridView_InfoShow.TabIndex = 2;
            // 
            // label_InfoShow_Select
            // 
            this.label_InfoShow_Select.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label_InfoShow_Select.AutoSize = true;
            this.label_InfoShow_Select.BackColor = System.Drawing.Color.Transparent;
            this.label_InfoShow_Select.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_InfoShow_Select.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_InfoShow_Select.Location = new System.Drawing.Point(311, 118);
            this.label_InfoShow_Select.Name = "label_InfoShow_Select";
            this.label_InfoShow_Select.Size = new System.Drawing.Size(115, 20);
            this.label_InfoShow_Select.TabIndex = 14;
            this.label_InfoShow_Select.Text = "Введіть ID:";
            // 
            // numericUpDown_InfoShow_Select
            // 
            this.numericUpDown_InfoShow_Select.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.numericUpDown_InfoShow_Select.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.numericUpDown_InfoShow_Select.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericUpDown_InfoShow_Select.Location = new System.Drawing.Point(442, 119);
            this.numericUpDown_InfoShow_Select.Name = "numericUpDown_InfoShow_Select";
            this.numericUpDown_InfoShow_Select.Size = new System.Drawing.Size(120, 22);
            this.numericUpDown_InfoShow_Select.TabIndex = 15;
            // 
            // button_InfoShow_Find
            // 
            this.button_InfoShow_Find.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button_InfoShow_Find.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button_InfoShow_Find.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_InfoShow_Find.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_InfoShow_Find.Location = new System.Drawing.Point(579, 119);
            this.button_InfoShow_Find.Name = "button_InfoShow_Find";
            this.button_InfoShow_Find.Size = new System.Drawing.Size(96, 22);
            this.button_InfoShow_Find.TabIndex = 16;
            this.button_InfoShow_Find.Text = "Select";
            this.button_InfoShow_Find.UseVisualStyleBackColor = false;
            this.button_InfoShow_Find.Click += new System.EventHandler(this.button_InfoShow_Find_Click);
            // 
            // Form_InfoShow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Football_Competitions.Properties.Resources.Background_Grass;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(984, 761);
            this.Controls.Add(this.button_InfoShow_Find);
            this.Controls.Add(this.numericUpDown_InfoShow_Select);
            this.Controls.Add(this.label_InfoShow_Select);
            this.Controls.Add(this.dataGridView_InfoShow);
            this.Controls.Add(this.label_InfoShow_Information);
            this.Controls.Add(this.menuStrip_InfoShow);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(800, 700);
            this.MainMenuStrip = this.menuStrip_InfoShow;
            this.MinimumSize = new System.Drawing.Size(800, 700);
            this.Name = "Form_InfoShow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Football — Перегляд";
            this.Shown += new System.EventHandler(this.Form_InfoShow_Shown);
            this.menuStrip_InfoShow.ResumeLayout(false);
            this.menuStrip_InfoShow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_InfoShow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_InfoShow_Select)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip_InfoShow;
        private System.Windows.Forms.ToolStripMenuItem Refresh1_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit1_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Info1_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit1_1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit1_2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit1_3ToolStripMenuItem;
        private System.Windows.Forms.Label label_InfoShow_Information;
        private System.Windows.Forms.DataGridView dataGridView_InfoShow;
        private System.Windows.Forms.ToolStripMenuItem AdminMode1_ToolStripMenuItem;
        private System.Windows.Forms.Label label_InfoShow_Select;
        private System.Windows.Forms.NumericUpDown numericUpDown_InfoShow_Select;
        private System.Windows.Forms.Button button_InfoShow_Find;
        private System.Windows.Forms.ToolStripMenuItem Search1_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Help1_ToolStripMenuItem;
    }
}