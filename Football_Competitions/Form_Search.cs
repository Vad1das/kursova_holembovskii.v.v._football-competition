﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Football_Competitions
{
    public partial class Form_Search : Form //Форма для пошуку за подібними символами
    {
        private User user;
        public Form_Search()
        {
            InitializeComponent();
        }

        //Database demonstrarion + Search
        public void searchData(string valueToSearch)
        {
            string _query = "SELECT * FROM `tournament` WHERE CONCAT(`tournamentName`, `1st place`, `2nd place`, `3rd place`, `tournamentInfo`) like '%" + valueToSearch + "%'";
            DataBase _manager = new DataBase();
            MySqlCommand _mySqlcommand = new MySqlCommand(_query, _manager.GetConnection());
            MySqlDataAdapter _mySqladapter = new MySqlDataAdapter(_mySqlcommand);
            DataTable _dataTable = new DataTable();
            _mySqladapter.Fill(_dataTable);
            dataGridView_Search.DataSource = _dataTable;
            dataGridView_Search.Columns[0].Width = 50;
            dataGridView_Search.Columns[1].Width = 200;
            dataGridView_Search.Columns[2].Width = 100;
            dataGridView_Search.Columns[3].Width = 100;
            dataGridView_Search.Columns[4].Width = 100;
            dataGridView_Search.Columns[5].Width = 400;
        }
        private void Form_Search_Load(object sender, EventArgs e)
        {
            user = new User();
            searchData("");
        }
        private void button_Search_Search_Click(object sender, EventArgs e)
        {
            string valueToSearch = textBox_Search_Search.Text.ToString();
            searchData(valueToSearch);
        }

        //ToolStrip - Info, Returns, Exits, Help
        private void Info3_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string infostring = "Інформаційно-пошукова система: футбольні змагання.\n" + "Звичайний користувач може переглядати, шукати інформацію. Адмін може її ще змінювати, видаляти.\n"
                + "Автор: Голембовський Вадим КІ-19-1[1].\n";
            MessageBox.Show(infostring, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void Return3_1_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_InfoShow form = new Form_InfoShow();
            this.Hide(); form.Show();
        }
        private void Return3_2_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (user.userAdmin == "true")
            {
                Form_InfoEdit form = new Form_InfoEdit();
                this.Hide(); form.Show();
            }
            else { MessageBox.Show("Ви не адмін!", "Помилка!", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }
        private void Exit3_1_ToolStripMenuItem_Click(object sender, EventArgs e) { Application.Exit(); }
        private void Exit3_2_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_SignIN form = new Form_SignIN();
            this.Hide(); form.Show();
        }
        private void Exit3_3_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_SignUP form = new Form_SignUP();
            this.Hide(); form.Show();
        }
        private void Help3_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string helpstring = "Це форма пошуку даних.\n" + "Для пошуку інформації просто введіть якийсь символ чи слово, " +
                "і тоді система видасть вам записи де наявний даний символ чи слово. Залежить від того, що ви ввели.\n" ;
            MessageBox.Show(helpstring, "Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
